import {Component, Injectable} from 'angular2/core';
import {BaseRequestOptions, Http, HTTP_PROVIDERS} from 'angular2/http';
import {DataService} from "./data.service";
import {Observable} from 'rxjs/Observable';
//import {MockBackend} from 'angular2/http/testing';

@Component({
    selector: 'my-app',
    templateUrl: 'templates/listings.html',
    providers: [DataService]
})


export class AppComponent {
results: string;
selectedItems: string;
selectCount: bool = false;

constructor (private _dataService: DataService) {

    //console.log('getting json mock data array')
    this._dataService.getResults()
     .subscribe(
        data => {
          this.results = data;
          console.log(data);
        },
        error => console.log(error)
     );

     //console.log('getting json mock data array')
     this._dataService.getSaved()
      .subscribe(
         data => {
           this.selectedItems = data;
           console.log(data);
         },
         error => console.log(error)
      );
 }

    onSelect(item) {
      //this.selectedItem = item;

      this.selectCount = false;

      if(this.selectedItems.indexOf(item) == -1){
        this.selectedItems.push(item)
        console.log (this.selectedItems);
      }

    };

    onDelete(item) {
      //if(this.results.indexOf(item) == -1){
        //console.log ('this item is not in the main list');
        //this.results.push (item)

          if(this.selectedItems.length == 1){
              this.selectCount = true;
          }
      //} console.log (item);
          //console.log (this.selectedItems.length);
          this.selectedItems.splice(this.selectedItems.indexOf(item),1);
          console.log (this.selectedItems);

    };

}
